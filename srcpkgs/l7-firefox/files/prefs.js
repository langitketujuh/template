user_pref("app.normandy.first_run", false);
user_pref("browser.tabs.inTitlebar", 0);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.toolbars.bookmarks.visibility", "never");
