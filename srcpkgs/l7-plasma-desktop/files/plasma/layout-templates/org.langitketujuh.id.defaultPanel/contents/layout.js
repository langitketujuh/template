var panel = new Panel
var panelScreen = panel.screen

panel.height = 2 * Math.floor(gridUnit * 2.5 / 2)

// Restrict horizontal panel to a maximum size of a 21:9 monitor
const maximumAspectRatio = 21/9;
if (panel.formFactor === "horizontal") {
    const geo = screenGeometry(panelScreen);
    const maximumWidth = Math.ceil(geo.height * maximumAspectRatio);

    if (geo.width > maximumWidth) {
        panel.alignment = "center";
        panel.minimumLength = maximumWidth;
        panel.maximumLength = maximumWidth;
    }
}

// The Kickoff launcher
var launcher = panel.addWidget("org.kde.plasma.kickoff")
launcher.currentConfigGroup = ["General"]
launcher.writeConfig("icon", "distributor-logo-langitketujuh")
launcher.writeConfig("lengthFirstMargin", 7)
launcher.writeConfig("favoritesDisplay", 0)
launcher.writeConfig("applicationsDisplay", 0)
launcher.writeConfig("favorites", "org.kde.dolphin.desktop,preferred://browser,ardour6.desktop,arduino.desktop,audacity.desktop,blender.desktop,cadence.desktop,carla.desktop,com.obsproject.Studio.desktop,displaycal.desktop.fontforge.FontForge.desktop,fr.handbrake.ghb.desktop,gimp.desktop,gmic_qt.desktop,godot.desktop,goxel.desktop,hugin.desktop,io.github.OpenToonz.desktop,kde.kid3-qt.desktop,librecad.desktop,lmms.desktop,net.fasterland.converseen.desktop,openscad.desktop,org.bunkus.mkvtoolnix-gui.desktop,org.freecad.FreeCAD.desktop,org.inkscape.Inkscape.desktop,org.kde.digikam.desktop,org.kde.kdenlive.desktop,org.kde.krita.desktop,org.synfig.SynfigStudio.desktop,rawtherapee.desktop,scribus.desktop,langitketujuh.system.upgrade.desktop")
launcher.currentConfigGroup = ["Shortcuts"]
launcher.writeConfig("global", "Alt+F1")

// Margin Separator
var separator = panel.addWidget("org.kde.plasma.marginsseparator")

// Icons-Only Task Manager
var tasks = panel.addWidget("org.kde.plasma.icontasks")
tasks.currentConfigGroup = ["General"]
tasks.writeConfig("fill", false)
tasks.writeConfig("iconSpacing", 0)
tasks.writeConfig("launchers", "applications:org.kde.dolphin.desktop,preferred://browser,applications:gimp.desktop,applications:org.inkscape.Inkscape.desktop,applications:libreoffice-startcenter.desktop,applications:org.telegram.desktop.desktop,applications:systemsettings.desktop,applications:langitketujuh.system.upgrade.desktop")
tasks.writeConfig("maxStripes", 1)
tasks.writeConfig("showOnlyCurrentDesktop", false)
tasks.writeConfig("showOnlyCurrentScreen", false)

// Add Left Expandable Spacer
var spacer = panel.addWidget("org.kde.plasma.panelspacer")

// Margin Separator
var separator = panel.addWidget("org.kde.plasma.marginsseparator")

var langIds = ["as",    // Assamese
"bn",    // Bengali
"bo",    // Tibetan
"brx",   // Bodo
"doi",   // Dogri
"gu",    // Gujarati
"hi",    // Hindi
"ja",    // Japanese
"kn",    // Kannada
"ko",    // Korean
"kok",   // Konkani
"ks",    // Kashmiri
"lep",   // Lepcha
"mai",   // Maithili
"ml",    // Malayalam
"mni",   // Manipuri
"mr",    // Marathi
"ne",    // Nepali
"or",    // Odia
"pa",    // Punjabi
"sa",    // Sanskrit
"sat",   // Santali
"sd",    // Sindhi
"si",    // Sinhala
"ta",    // Tamil
"te",    // Telugu
"th",    // Thai
"ur",    // Urdu
"vi",    // Vietnamese
"zh_CN", // Simplified Chinese
"zh_TW"] // Traditional Chinese

if (langIds.indexOf(languageId) != -1) {
    panel.addWidget("org.kde.plasma.kimpanel");
}

// Color Picker
panel.addWidget("org.kde.plasma.colorpicker")

// System Tray
panel.addWidget("org.kde.plasma.systemtray")

// Digital Clock
var digitalclock = panel.addWidget("org.kde.plasma.digitalclock")
digitalclock.currentConfigGroup = ["Appearance"]
digitalclock.writeConfig("showDate", false)
digitalclock.writeConfig("use24hFormat", 0)
digitalclock.writeConfig("showWeekNumbers", true)

// User Switcher
var switcher = panel.addWidget("org.kde.plasma.userswitcher")
switcher.currentConfigGroup = ["General"]
switcher.writeConfig("showFace", true)
switcher.writeConfig("showName", false)
switcher.writeConfig("showTechnicalInfo", true)
